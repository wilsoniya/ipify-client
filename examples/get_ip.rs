extern crate futures;
extern crate hyper;
extern crate ipify_client;

use futures::Future;
use hyper::rt;

fn main() {
        let future = ipify_client::get_ip()
            .map(|ip| {
                println!("This machine's IP address: {}", ip);
            })
            .map_err(|e| {
                eprintln!("failed to successfully resolve future: {:?}", e)
            });
    rt::run(future);
}
