# ipify-client

A Rust client interface to the ipify.org IP resolution service.

## Usage

```
[dependencies]
ipify-client = "0.1.0"
```

```rust
extern crate futures;
extern crate hyper;
extern crate ipify_client;

use futures::Future;
use hyper::rt;

fn main() {
        let future = ipify_client::get_ip()
            .map(|ip| {
                println!("This machine's IP address: {}", ip);
            })
            .map_err(|e| {
                eprintln!("failed to successfully resolve future: {:?}", e)
            });
    rt::run(future);
}
```

Prints, e.g.:

```
This machine's IP address: 1.2.3.4
```
