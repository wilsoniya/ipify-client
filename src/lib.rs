//! Rusty interface to ipify.org

extern crate futures;
extern crate hyper;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

mod ipify;

pub use ipify::{IP, IpifyError, get_ip};
